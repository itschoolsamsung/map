import java.util.*;

public class Test {
    public static void main(String[] args) {

        /*
         * HashMap
         */

        HashMap<String, Integer> m = new HashMap<>();
        m.put("понедельник", 1);
        m.put("вторник", 2);
        m.put("среда", 3);

        System.out.println("keySet: " + m.keySet());
        System.out.println("values: " + m.values());
        System.out.println(m.get("вторник"));

        System.out.println(m.containsKey("понедельник"));
        System.out.println(m.containsValue(3));
        System.out.println(m.size());

        for (Map.Entry e : m.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }

        Iterator i = m.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry e = (Map.Entry)i.next();
            System.out.println(e.getKey() + " " + e.getValue());
        }

        m.entrySet().forEach( e -> System.out.println(e.getKey() + " " + e.getValue()) ); //Java 8+

        /*
         * TreeMap
         */

        TreeMap<Integer, String> t = new TreeMap<>();
        t.put(3, "март");
        t.put(1, "январь");
        t.put(2, "февраль");
        System.out.println(t);

        class MyInteger implements Comparable {
            Integer i;

            MyInteger(Integer i)
            {
                this.i = i;
            }

            @Override
            public int compareTo(Object o) {
                return -1 * i.compareTo(((MyInteger)o).i);
            }

            @Override
            public String toString() {
                return Integer.toString(i);
            }
        }

        TreeMap<MyInteger, String> s = new TreeMap<>();
        s.put(new MyInteger(3), "март");
        s.put(new MyInteger(1), "январь");
        s.put(new MyInteger(2), "февраль");
        System.out.println(s);
    }
}
